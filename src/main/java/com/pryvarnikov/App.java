package com.pryvarnikov;

import com.pryvarnikov.exceptions.FilePropertiesException;
import com.pryvarnikov.exceptions.InvalidNumberException;
import com.pryvarnikov.exceptions.InvalidTableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * This class starts the multiplication table application.
 * sdf
 * The properties file must be in the same directory as the running class or jar archive.
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        logger.info("Application start");
        try {
//            Starter.start("src/main/resources/app.properties");
            Starter.start("app.properties");
        } catch (FilePropertiesException | InvalidNumberException | InvalidTableException e) {
            logger.error(e.getMessage());
            logger.debug(Arrays.toString(e.getStackTrace()));
        }
    }
}
