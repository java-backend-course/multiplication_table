package com.pryvarnikov;

import com.pryvarnikov.exceptions.InvalidNumberException;
import com.pryvarnikov.exceptions.InvalidTableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MultiplicationTable {
    private final Number min, max, increment;
    String typeOfNumber;
    private static final Logger logger = LoggerFactory.getLogger(MultiplicationTable.class);

    public MultiplicationTable(Number min, Number max, Number increment, String typeOfNumber) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.typeOfNumber = typeOfNumber;
    }

    private void checkArguments() throws InvalidNumberException {
        logger.info("Validating arguments");
        switch (typeOfNumber) {
            case "double":
                if (min.doubleValue() > max.doubleValue()) {
                    throw new InvalidNumberException("The minimum number is greater than the maximum");
                } else if (increment.doubleValue() < 0 || increment.doubleValue() == 0) {
                    throw new InvalidNumberException("Negative or zero increment");
                }
                break;
            case "int":
                if (min.intValue() > max.intValue()) {
                    throw new InvalidNumberException("The minimum number is greater than the maximum");
                } else if (increment.intValue() < 0 || increment.intValue() == 0) {
                    throw new InvalidNumberException("Negative or zero increment");
                }
                break;
            case "byte":
                if (min.byteValue() > max.byteValue()) {
                    throw new InvalidNumberException("The minimum number is greater than the maximum");
                } else if (increment.byteValue() < 0 || increment.byteValue() == 0) {
                    throw new InvalidNumberException("Negative or zero increment");
                }
                break;
            case "short":
                if (min.shortValue() > max.shortValue()) {
                    throw new InvalidNumberException("The minimum number is greater than the maximum");
                } else if (increment.shortValue() < 0 || increment.byteValue() == 0) {
                    throw new InvalidNumberException("Negative or zero increment");
                }
                break;
            case "long":
                if (min.longValue() > max.longValue()) {
                    throw new InvalidNumberException("The minimum number is greater than the maximum");
                } else if (increment.longValue() < 0 || increment.longValue() == 0) {
                    throw new InvalidNumberException("Negative or zero increment");
                }
                break;
            default:
                throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
        }
    }

    public Number[][] generateTable() throws InvalidNumberException {
        logger.info("Starting table generation");
        checkArguments();
        int size = getSize();
        Number[][] table = new Number[size][size];
        Number yNumber, xNumber;
        yNumber = min;
        for (int i = 0; i < size; i++) {
            xNumber = min;
            for (int j = 0; j < size; j++) {
                table[i][j] = calculateMultiplication(yNumber, xNumber);
                xNumber = calculateSum(xNumber, increment);
            }
            yNumber = calculateSum(yNumber, increment);
        }
        logger.info("Table successfully generated");
        return table;
    }

    private int getSize() throws InvalidNumberException {
        int size;
        switch (typeOfNumber) {
            case "double":
                size = (int) (((max.doubleValue() - min.doubleValue()) / increment.doubleValue()) + 1);
                break;
            case "int":
                size = ((max.intValue() - min.intValue()) / increment.intValue()) + 1;
                break;
            case "byte":
                size = ((max.byteValue() - min.byteValue()) / increment.byteValue()) + 1;
                break;
            case "short":
                size = ((max.shortValue() - min.shortValue()) / increment.shortValue()) + 1;
                break;
            case "long":
                size = (int) (((max.longValue() - min.longValue()) / increment.longValue()) + 1);
                break;
            default:
                throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
        }
        return size;
    }

    private Number calculateSum(Number firstNumber, Number secondNumber) throws InvalidNumberException {
        Number sum;
        switch (typeOfNumber) {
            case "double":
                sum = firstNumber.doubleValue() + secondNumber.doubleValue();
                break;
            case "int":
                sum = firstNumber.intValue() + secondNumber.intValue();
                break;
            case "byte":
                sum = firstNumber.byteValue() + secondNumber.byteValue();
                break;
            case "long":
                sum = firstNumber.longValue() + secondNumber.longValue();
                break;
            case "short":
                sum = firstNumber.shortValue() + secondNumber.shortValue();
                break;
            default:
                throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
        }
        return sum;
    }

    private Number calculateMultiplication(Number firstNumber, Number secondNumber) throws InvalidNumberException {
        Number multiplication;
        switch (typeOfNumber) {
            case "double":
                multiplication = firstNumber.doubleValue() * secondNumber.doubleValue();
                break;
            case "int":
                multiplication = firstNumber.intValue() * secondNumber.intValue();
                break;
            case "byte":
                multiplication = firstNumber.byteValue() * secondNumber.byteValue();
                break;
            case "long":
                multiplication = firstNumber.longValue() * secondNumber.longValue();
                break;
            case "short":
                multiplication = firstNumber.shortValue() * secondNumber.shortValue();
                break;
            default:
                throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
        }
        return multiplication;
    }

    public void printToConsole(Number[][] table) throws InvalidTableException, InvalidNumberException {
        logger.info("Printing a table to the console");
        if (table == null) {
            throw new InvalidTableException("Table not found. Try to create it first");
        }
        Number xScale = min, yScale = min;

        if (typeOfNumber.equals("double")) {
            System.out.print("\t");
        }

        for (int i = 0; i < table.length; i++) {
            switch (typeOfNumber) {
                case "int":
                    System.out.printf("\t%s", xScale.intValue());
                    break;
                case "double":
                    System.out.printf("\t%s", BigDecimal.valueOf(xScale.doubleValue()).setScale(2, RoundingMode.HALF_UP));
                    break;
                case "byte":
                    System.out.printf("\t%s", xScale.byteValue());
                    break;
                case "long":
                    System.out.printf("\t%s", xScale.longValue());
                    break;
                case "short":
                    System.out.printf("\t%s", xScale.shortValue());
                    break;
                default:
                    throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
            }
            xScale = calculateSum(xScale, increment);
        }

        System.out.println();
        for (Number[] numberY : table) {
            switch (typeOfNumber) {
                case "int":
                    System.out.printf("%s\t", yScale.intValue());
                    break;
                case "double":
                    System.out.printf("%s\t", BigDecimal.valueOf(yScale.doubleValue()).setScale(2, RoundingMode.HALF_UP));
                    break;
                case "short":
                    System.out.printf("%s\t", yScale.shortValue());
                    break;
                case "long":
                    System.out.printf("%s\t", yScale.longValue());
                    break;
                case "byte":
                    System.out.printf("%s\t", yScale.byteValue());
                    break;
                default:
                    throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
            }

            for (Number numberX : numberY) {
                switch (typeOfNumber) {
                    case "int":
                        System.out.printf("%s\t", numberX.intValue());
                        break;
                    case "double":
                        System.out.printf("%s\t", BigDecimal.valueOf(numberX.doubleValue()).setScale(2, RoundingMode.HALF_UP));
                        break;
                    case "short":
                        System.out.printf("%s\t", numberX.shortValue());
                        break;
                    case "long":
                        System.out.printf("%s\t", numberX.longValue());
                        break;
                    case "byte":
                        System.out.printf("%s\t", numberX.byteValue());
                        break;
                    default:
                        throw new InvalidNumberException("Incorrect number type: " + typeOfNumber);
                }
            }
            System.out.println();
            yScale = calculateSum(yScale, increment);
        }
    }
}
