package com.pryvarnikov;

import com.pryvarnikov.exceptions.FilePropertiesException;
import com.pryvarnikov.exceptions.InvalidNumberException;
import com.pryvarnikov.exceptions.InvalidTableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Properties;

public class Starter {
    private static final Logger logger = LoggerFactory.getLogger(Starter.class);

    public static void start(String propertiesFileName) throws FilePropertiesException, InvalidNumberException, InvalidTableException {
        Properties appProps = new Properties();
        try {
            appProps.load(new InputStreamReader(new FileInputStream(propertiesFileName), StandardCharsets.UTF_8));
            logger.info("Properties file loaded successfully");
        } catch (IOException e) {
            throw new FilePropertiesException(String.format("File %s not found", propertiesFileName));
        }

        checkProperties(appProps);

        String type = System.getenv("TYPE");
        if (type == null || type.isEmpty()) {
            type = "int";
        } else {
            type = type.toLowerCase();
        }

        MultiplicationTable mt;
        try {
            mt = new MultiplicationTable(
                    NumberFormat.getInstance().parse(appProps.getProperty("min")),
                    NumberFormat.getInstance().parse(appProps.getProperty("max")),
                    NumberFormat.getInstance().parse(appProps.getProperty("increment")),
                    type);
        } catch (ParseException e) {
            throw new InvalidNumberException(String.format("%s in %s file", e.getMessage(), propertiesFileName));
        }
        Number[][] table = mt.generateTable();
        mt.printToConsole(table);
        logger.info("Application completed successfully");
    }

    private static void checkProperties(Properties appProps) throws FilePropertiesException {
        StringBuilder missingProperties = new StringBuilder();
        if (!appProps.containsKey("min")) {
            missingProperties.append("'min' ");
        }
        if (!appProps.containsKey("max")) {
            missingProperties.append("'max' ");
        }
        if (!appProps.containsKey("increment")) {
            missingProperties.append("'increment' ");
        }
        if (missingProperties.length() != 0) {
            throw new FilePropertiesException(String
                    .format("The following values are missing from the properties file: %s", missingProperties));
        }
    }
}
