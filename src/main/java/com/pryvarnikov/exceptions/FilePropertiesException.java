package com.pryvarnikov.exceptions;

public class FilePropertiesException extends Exception {
    public FilePropertiesException(String message) {
        super(message);
    }
}
