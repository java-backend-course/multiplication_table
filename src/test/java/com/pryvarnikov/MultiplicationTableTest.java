package com.pryvarnikov;

import com.pryvarnikov.exceptions.InvalidNumberException;
import com.pryvarnikov.exceptions.InvalidTableException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTableTest {
    private MultiplicationTable multiplicationTable;
    private Number[][] tableActual;

    @Test
    void checkArgumentsWhenMinGreaterMax() {
        multiplicationTable = new MultiplicationTable(100, 10, 1, "double");
        Exception exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("The minimum number is greater than the maximum", exception.getMessage());

        multiplicationTable = new MultiplicationTable(100, 10, 1, "int");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("The minimum number is greater than the maximum", exception.getMessage());

        multiplicationTable = new MultiplicationTable(100, 10, 1, "byte");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("The minimum number is greater than the maximum", exception.getMessage());

        multiplicationTable = new MultiplicationTable(100, 10, 1, "short");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("The minimum number is greater than the maximum", exception.getMessage());

        multiplicationTable = new MultiplicationTable(100, 10, 1, "long");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("The minimum number is greater than the maximum", exception.getMessage());
    }

    @Test
    void checkArgumentsWhenInvalidIncrement() {
        multiplicationTable = new MultiplicationTable(1, 10, 0, "double");
        Exception exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(1, 10, -10, "double");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(1, 103, 0, "int");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(6, 10, -134, "int");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(-6, 103, 0, "byte");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(4, 6, -1, "byte");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(23, 425, 0, "short");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(4, 6, -5, "short");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(-23, 0, 0, "long");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());

        multiplicationTable = new MultiplicationTable(48234, 48300, -923, "long");
        exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Negative or zero increment", exception.getMessage());
    }

    @Test
    void checkArgumentsWhenInvalidNumberType() {
        multiplicationTable = new MultiplicationTable(1, 10, 0, "wrong type");
        Exception exception = assertThrows(InvalidNumberException.class, () -> multiplicationTable.generateTable());
        assertEquals("Incorrect number type: wrong type", exception.getMessage());
    }

    @Test
    void checkIntTable() throws InvalidNumberException {
        int[][] tableExpected = new int[][]{
                {4, 2, 0, -2, -4, -6, -8, -10},
                {2, 1, 0, -1, -2, -3, -4, -5},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {-2, -1, 0, 1, 2, 3, 4, 5},
                {-4, -2, 0, 2, 4, 6, 8, 10},
                {-6, -3, 0, 3, 6, 9, 12, 15},
                {-8, -4, 0, 4, 8, 12, 16, 20},
                {-10, -5, 0, 5, 10, 15, 20, 25}
        };

        multiplicationTable = new MultiplicationTable(-2, 5, 1, "int");
        tableActual = multiplicationTable.generateTable();

        for (int i = 0; i < tableExpected.length; i++) {
            for (int j = 0; j < tableExpected[i].length; j++) {
                assertEquals(tableExpected[i][j], tableActual[i][j].intValue());
            }
        }
    }

    @Test
    void checkDoubleTable() throws InvalidNumberException {
        double[][] tableExpected = new double[][]{
                {28.09, 29.68, 31.27, 32.86, 34.45, 36.04},
                {29.68, 31.36, 33.04, 34.72, 36.40, 38.08},
                {31.27, 33.04, 34.81, 36.58, 38.35, 40.12},
                {32.86, 34.72, 36.58, 38.44, 40.30, 42.16},
                {34.45, 36.40, 38.35, 40.30, 42.25, 44.20},
                {36.04, 38.08, 40.12, 42.16, 44.20, 46.24}
        };

        multiplicationTable = new MultiplicationTable(5.3, 7, 0.3, "double");
        tableActual = multiplicationTable.generateTable();

        for (int i = 0; i < tableExpected.length; i++) {
            for (int j = 0; j < tableExpected[i].length; j++) {
                assertEquals(BigDecimal.valueOf(tableExpected[i][j]).setScale(2, RoundingMode.HALF_UP),
                        BigDecimal.valueOf(tableActual[i][j].doubleValue()).setScale(2, RoundingMode.HALF_UP));
            }
        }
    }

    @Test
    void checkLongTable() throws InvalidNumberException {
        long[][] tableExpected = new long[][]{
                {8100000000L, 8255160000L, 8410320000L, 8565480000L, 8720640000L, 8875800000L},
                {8255160000L, 8413292176L, 8571424352L, 8729556528L, 8887688704L, 9045820880L},
                {8410320000L, 8571424352L, 8732528704L, 8893633056L, 9054737408L, 9215841760L},
                {8565480000L, 8729556528L, 8893633056L, 9057709584L, 9221786112L, 9385862640L},
                {8720640000L, 8887688704L, 9054737408L, 9221786112L, 9388834816L, 9555883520L},
                {8875800000L, 9045820880L, 9215841760L, 9385862640L, 9555883520L, 9725904400L}
        };

        multiplicationTable = new MultiplicationTable(90000, 100000, 1724, "long");
        tableActual = multiplicationTable.generateTable();

        for (int i = 0; i < tableExpected.length; i++) {
            for (int j = 0; j < tableExpected[i].length; j++) {
                assertEquals(tableExpected[i][j], tableActual[i][j].longValue());
            }
        }
    }

    @Test
    void checkByteTable() throws InvalidNumberException {
        byte[][] tableExpected = new byte[][]{
                {4, 8, 12, 16, 20},
                {8, 16, 24, 32, 40},
                {12, 24, 36, 48, 60},
                {16, 32, 48, 64, 80},
                {20, 40, 60, 80, 100}
        };

        multiplicationTable = new MultiplicationTable(2, 11, 2, "byte");
        tableActual = multiplicationTable.generateTable();

        for (int i = 0; i < tableExpected.length; i++) {
            for (int j = 0; j < tableExpected[i].length; j++) {
                assertEquals(tableExpected[i][j], tableActual[i][j].byteValue());
            }
        }
    }

    @Test
    void checkShortTable() throws InvalidNumberException {
        short[][] tableExpected = new short[][]{
                {64, 40, 16, -8, -32, -56, -80, -104, -128, -152},
                {40, 25, 10, -5, -20, -35, -50, -65, -80, -95},
                {16, 10, 4, -2, -8, -14, -20, -26, -32, -38},
                {-8, -5, -2, 1, 4, 7, 10, 13, 16, 19},
                {-32, -20, -8, 4, 16, 28, 40, 52, 64, 76},
                {-56, -35, -14, 7, 28, 49, 70, 91, 112, 133},
                {-80, -50, -20, 10, 40, 70, 100, 130, 160, 190},
                {-104, -65, -26, 13, 52, 91, 130, 169, 208, 247},
                {-128, -80, -32, 16, 64, 112, 160, 208, 256, 304},
                {-152, -95, -38, 19, 76, 133, 190, 247, 304, 361}
        };

        multiplicationTable = new MultiplicationTable(-8, 20, 3, "short");
        tableActual = multiplicationTable.generateTable();

        for (int i = 0; i < tableExpected.length; i++) {
            for (int j = 0; j < tableExpected[i].length; j++) {
                assertEquals(tableExpected[i][j], tableActual[i][j].shortValue());
            }
        }
    }

    @Test
    void checkInvalidTableException() {
        multiplicationTable = new MultiplicationTable(1, 10, 1, "int");
        Exception exception = assertThrows(InvalidTableException.class, () -> multiplicationTable.printToConsole(null));
        assertEquals("Table not found. Try to create it first", exception.getMessage());
    }

    @Test
    void checkPrintToConsole() throws InvalidNumberException, InvalidTableException {
        multiplicationTable = new MultiplicationTable(1, 10, 1, "int");
        tableActual = multiplicationTable.generateTable();
        multiplicationTable.printToConsole(tableActual);

        multiplicationTable = new MultiplicationTable(1, 10, 1, "double");
        multiplicationTable.printToConsole(tableActual);

        multiplicationTable = new MultiplicationTable(1, 10, 1, "byte");
        multiplicationTable.printToConsole(tableActual);

        multiplicationTable = new MultiplicationTable(1, 10, 1, "long");
        multiplicationTable.printToConsole(tableActual);

        multiplicationTable = new MultiplicationTable(1, 10, 1, "short");
        multiplicationTable.printToConsole(tableActual);
    }
}