package com.pryvarnikov;

import com.pryvarnikov.exceptions.FilePropertiesException;
import com.pryvarnikov.exceptions.InvalidNumberException;
import com.pryvarnikov.exceptions.InvalidTableException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SystemStubsExtension.class)
class StarterTest {

    @Test
    void checkWithNonExistentPropertiesFile() {
        Exception exception = assertThrows(FilePropertiesException.class, () -> Starter.start("non-existentFile"));
        assertEquals("File non-existentFile not found", exception.getMessage());
    }

    @Test
    void checkPropertiesWithMissingKeys() {
        Exception exception = assertThrows(FilePropertiesException.class, () -> Starter.start("src/test/resources/propertiesEmpty"));
        assertEquals("The following values are missing from the properties file: 'min' 'max' 'increment' ", exception.getMessage());
    }

    @Test
    void checkSystemEnvNull() throws FilePropertiesException, InvalidNumberException, InvalidTableException {
        Starter.start("src/test/resources/propertiesOk");
    }

    @Test
    void checkSystemEnvEmpty(EnvironmentVariables environment) throws FilePropertiesException, InvalidNumberException, InvalidTableException {
        environment.set("TYPE", "");
        Starter.start("src/test/resources/propertiesOk");
    }

    @Test
    void checkSystemEnvIsNotDefault(EnvironmentVariables environment) throws FilePropertiesException, InvalidNumberException, InvalidTableException {
        environment.set("TYPE", "DoUbLe");
        Starter.start("src/test/resources/propertiesOk");
    }

    @Test
    void checkParseException() {
        Exception exception = assertThrows(InvalidNumberException.class, () -> Starter.start("src/test/resources/propertiesFailParse"));
        assertEquals("Unparseable number: \"some_number\" in src/test/resources/propertiesFailParse file", exception.getMessage());
    }
}